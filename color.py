#Thomas Reed 
#c o l o r

def decreaseRed(img):
  for pixel in getPixels(img):
    redVal = getRed(pixel) /  2
    setRed(pixel, redVal)

mypic = '/Users/treed2017/Documents/python/week3/potato.jpg'

picture = makePicture(mypic)

decreaseRed(picture)

show(picture)

